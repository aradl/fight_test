﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace fight_test
{
    class PlayerSpriteController
    {
        private enum Pose
        {
            front = 0,
            back  = 1,
            left  = 2,
            right = 3
        }

        // control
        private string type;
        private float idle_delta_time;
        private float attack_delta_time;
        private Pose a_pose, pose;
        private const float idle_anim_cycle = 0.08f;   // in seconds
        private const float attack_anim_cycle = 0.2f; // in seconds
        //
        private bool start_attack_anim = false;
        public bool is_attack_anim     = false;

        // player sprites
        private Texture2D tex_front; //TODO dictionary of textures?
        private Texture2D tex_back;
        private Texture2D tex_left;
        private Texture2D tex_right;

        private Texture2D tex_attack_front;
        private Texture2D tex_attack_back;
        private Texture2D tex_attack_left;
        private Texture2D tex_attack_right;

        private Texture2D tex_idle_animA;
        private Texture2D tex_idle_animB;

        // sprite pack sizes
        private const int robot_size = 32 + 32 / 2;
        public Vector2 Size { get; private set; }

        // active sprites
        private Texture2D current_idle;
        private Texture2D current_tex;        

        public PlayerSpriteController(string _type)
        {
            // init
            type   = _type;
            pose   = Pose.front;
            a_pose = pose;

            // select based on type
            switch (type)
            {
                case "robot":
                    Size = new Vector2(robot_size);
                    break;
                default:
                    break;
            }
        }

        public void LoadContent(Microsoft.Xna.Framework.Content.ContentManager CM)
        {
            // select based on type
            switch (type)
            {
                case "robot":
                    // main textures
                    tex_front = CM.Load<Texture2D>("characters\\robot\\robot_front");
                    tex_back  = CM.Load<Texture2D>("characters\\robot\\robot_back");
                    tex_left  = CM.Load<Texture2D>("characters\\robot\\robot_sideL");
                    tex_right = CM.Load<Texture2D>("characters\\robot\\robot_sideR");

                    // attack textures
                    tex_attack_front = CM.Load<Texture2D>("characters\\robot\\robot_front_shooting");
                    tex_attack_back  = CM.Load<Texture2D>("characters\\robot\\robot_back_shooting");
                    tex_attack_left  = CM.Load<Texture2D>("characters\\robot\\robot_sideL_shooting");
                    tex_attack_right = CM.Load<Texture2D>("characters\\robot\\robot_sideR_shooting");

                    // idle animation textures
                    tex_idle_animA = CM.Load<Texture2D>("characters\\robot\\robot_flameA");
                    tex_idle_animB = CM.Load<Texture2D>("characters\\robot\\robot_flameB");

                    // init textures
                    current_tex  = tex_front;
                    current_idle = tex_idle_animA;
                    break;
                default:
                    break;
            }
        }

        public void ChangeState(string command)
        {
            // update to keep track of last state for attacking, unless 
            // you find a better structure
            switch (command)
            {
                case "front":                    
                    pose = Pose.front;
                    this.ChangeTexPose();
                    break;

                case "back":
                    pose = Pose.back;
                    this.ChangeTexPose();
                    break;

                case "left":
                    pose = Pose.left;
                    this.ChangeTexPose();
                    break;

                case "right":
                    pose = Pose.right;
                    this.ChangeTexPose();
                    break;

                case "attack":
                    if (!is_attack_anim)
                    {
                        start_attack_anim = true;
                        is_attack_anim    = true;
                        a_pose            = pose;
                    }
                    break;

                default:
                    break;
            }
        }

        private void ChangeTexPose()
        {
            switch (pose)
            {
                case Pose.front:
                    current_tex = tex_front;
                    break;

                case Pose.back:
                    current_tex = tex_back;
                    break;

                case Pose.left:
                    current_tex = tex_left;
                    break;

                case Pose.right:
                    current_tex = tex_right;
                    break;
            }
        }

        public void Update(GameTime gameTime)
        {
            // attack animation
            if (is_attack_anim)
            {
                attack_delta_time += (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (start_attack_anim)
                {
                    switch (a_pose)
                    {
                        case Pose.front:
                            current_tex = tex_attack_front;
                            break;
                        case Pose.back:
                            current_tex = tex_attack_back;
                            break;
                        case Pose.left:
                            current_tex = tex_attack_left;
                            break;
                        case Pose.right:
                            current_tex = tex_attack_right;
                            break;
                    }
                    start_attack_anim = false;
                }

                if(attack_delta_time >= attack_anim_cycle)
                {
                    attack_delta_time = 0f;
                    is_attack_anim    = false;
                    this.ChangeTexPose();
                }
            }

            // cycles through idle animation images          
            idle_delta_time += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (idle_delta_time >= idle_anim_cycle)
            {
                idle_delta_time = 0f;
                if (current_idle == tex_idle_animA)
                {
                    current_idle = tex_idle_animB;
                }
                else
                {
                    current_idle = tex_idle_animA;
                }
            }
        }

        public void Draw(SpriteBatch SB, Vector2 location)
        {
            Rectangle rect = new Rectangle((int)location.X, (int)location.Y, (int)Size.X, (int)Size.Y);

            SB.Draw(current_tex, rect, new Color(Color.White, 1.0f));
            SB.Draw(current_idle, rect, new Color(Color.White, 1.0f));
        }
    }
}