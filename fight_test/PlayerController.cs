﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace fight_test
{
    /// <summary>
    /// Summary description for Class1
    /// </summary>
    class PlayerController
    {
        // sprite
        private PlayerSpriteController sprite_pack;
        private SfxController sfx;

        // control
        private Vector2 location;
        private Vector2 pLocation;        

        // player move attributes
        private const int player_move_amount = 5;

        public PlayerController(Vector2 starting_location, string type)
        {
            sprite_pack = new PlayerSpriteController(type);
            //sfx         = new SfxController(0.1f);
            //
            location = starting_location;
        }

        public void LoadContent(Microsoft.Xna.Framework.Content.ContentManager CM)
        {
            sprite_pack.LoadContent(CM);
            //sfx.LoadContent(CM);
        }

        public void Update(GamePadState gamePadState, KeyboardState keyBoardState, GameTime gameTime, LevelGenerator lvl)
        {
            // save last location
            pLocation = location;

            // move player
            //sfx.playIdle();
            if (!sprite_pack.is_attack_anim)
            {
                this.Move(gamePadState, keyBoardState);
            }

            // check collision
            if(lvl.CheckCollision(new Rectangle(location.ToPoint(), sprite_pack.Size.ToPoint() ))){
                location = pLocation;
            }

            // check transision
            if(lvl.CheckTransision(new Rectangle(location.ToPoint(), sprite_pack.Size.ToPoint() ))){
                location = lvl.Starting_location;
            }

            // action
            this.Action(gamePadState, keyBoardState);

            // update sprite
            sprite_pack.Update(gameTime);
        }

        private void Move(GamePadState gamePadState, KeyboardState keyBoardState)
        {
            // up
            if (keyBoardState.IsKeyDown(Keys.W) || gamePadState.IsButtonDown(Buttons.X)) //TODO parameterize controlls
            {
                location += new Vector2(0, -player_move_amount);
                sprite_pack.ChangeState("back");
                //sfx.playMove();
            }

            // down
            if (keyBoardState.IsKeyDown(Keys.S) || gamePadState.IsButtonDown(Buttons.B))
            {
                location += new Vector2(0, player_move_amount);
                sprite_pack.ChangeState("front");
                //sfx.playMove();
            }

            // left
            if (keyBoardState.IsKeyDown(Keys.A) || gamePadState.IsButtonDown(Buttons.Y))
            {
                location += new Vector2(-player_move_amount, 0);
                sprite_pack.ChangeState("left");
                //sfx.playMove();
            }

            // right
            if (keyBoardState.IsKeyDown(Keys.D) || gamePadState.IsButtonDown(Buttons.A))
            {
                location += new Vector2(player_move_amount, 0);
                sprite_pack.ChangeState("right");
                //sfx.playMove();
            }
        }

        private void Action(GamePadState gamePadState, KeyboardState keyBoardState)
        {
            // attack
            if (keyBoardState.IsKeyDown(Keys.Space))
            {
                sprite_pack.ChangeState("attack");
            }
        }

        public void Draw(SpriteBatch SB)
        {
            sprite_pack.Draw(SB, location);

        }
    }
}
