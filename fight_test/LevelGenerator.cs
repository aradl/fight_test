﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System.Collections.Generic;

namespace fight_test
{
    class LevelGenerator
    {
        private GameState gs;

        // level size
        private const int size_tile = 32;
        private const int level_size_x = 16 * 1;
        private const int level_size_y = 9 * 1;
        private const int size_x = size_tile * level_size_x;
        private const int size_y = size_tile * level_size_y;
        private const int max_tile_amount = level_size_x + level_size_y;

        // offsets
        private const int level_offset = 1;
        private const int border_size  = 1;
        private const int place_offset_x = level_size_x + level_offset + border_size;
        private const int place_offset_y = level_size_y + level_offset + border_size;

        // tiles
        private Texture2D tile_basic;
        private Texture2D tile_grey;
        private Texture2D tile_blue;
        private Texture2D tile_pink;

        // level event spaces
        private List<Vector2> collision_tiles;
        private List<Vector2> transision_tiles;

        // public vars
        public Vector2 Starting_location { get; private set; }

        public LevelGenerator(GameState _gs)
        {
            gs = _gs;

            collision_tiles   = new List<Vector2>();
            transision_tiles  = new List<Vector2>();
            Starting_location = new Vector2((int)size_x/2, (int)size_y/2);
        }


        public void LoadContent(Microsoft.Xna.Framework.Content.ContentManager CM)
        {
            tile_basic = CM.Load<Texture2D>("tile_basic");
            tile_grey  = CM.Load<Texture2D>("tile_grey");
            tile_blue  = CM.Load<Texture2D>("tile_blue");
            tile_pink  = CM.Load<Texture2D>("tile_pink");
        }

        public Boolean CheckCollision(Rectangle coll_object)
        {
            foreach (Vector2 tile in collision_tiles)
            {                 
                if (coll_object.Intersects(new Rectangle(new Point((int)tile.X * size_tile, (int)tile.Y * size_tile) , new Point(size_tile)))) {
                    return true;
                }
            }

            return false;
        }

        public Boolean CheckTransision(Rectangle coll_object)
        {
            foreach (Vector2 tile in transision_tiles)
            {
                if (coll_object.Intersects(new Rectangle(new Point((int)tile.X * size_tile, (int)tile.Y * size_tile), new Point(size_tile))))
                {
                    this.LevelTransion(tile);
                    return true;
                }
            }

            return false;
        }

        private void PlaceTile(Texture2D tex, SpriteBatch SB, Vector2 pos)
        {            
            SB.Draw(tex, new Rectangle(size_tile * (int)pos.X, size_tile * (int)pos.Y, size_tile, size_tile), new Color(Color.White, 0.8f));
        }

        private void FillLvlWith(Texture2D fill_tex, Texture2D bor_tex, SpriteBatch SB)
        {
            // offset and fill            
            for (int ix = level_offset; ix <= place_offset_x; ix++)
            {
                for (int iy = level_offset; iy <= place_offset_y; iy++)
                {
                    // isolate boarder reagion
                    if ((ix <= border_size || iy <= border_size || ix > place_offset_x - border_size || iy > place_offset_y - border_size) && bor_tex != null)
                    {
                        PlaceTile(bor_tex, SB, new Vector2(ix, iy));
                        collision_tiles.Add(new Vector2(ix, iy));
                    } 
                    else
                    {
                        PlaceTile(fill_tex, SB, new Vector2(ix, iy));
                    }                    
                }
            }
        }

        private void LevelTransion(Vector2 trig)
        {            
            switch (gs.current_map)
            {
                case Map.home:
                    // only 1 transision
                    gs.current_map = Map.level_1_1;
                    Starting_location = new Vector2((level_offset + 2) * size_tile, (int)(place_offset_y / 2) * size_tile); // TODO the +2 needs to related to the characters size

                    break;
                case Map.level_1_1:
                    // only 1 transision
                    gs.current_map = Map.home;
                    Starting_location = new Vector2((place_offset_x - 2) * size_tile, (int)(place_offset_y / 2) * size_tile); // TODO think of a better way to keep track of this

                    break;
                case Map.level_1_2:
                    break;
                case Map.level_1_3:
                    break;
            }
        }

        private void DrawMap(SpriteBatch SB)
        {
            switch (gs.current_map)
            {
                case Map.home:
                    collision_tiles.Clear(); //TODO stop setting these Lists everytime you draw!!!
                    transision_tiles.Clear();

                    this.FillLvlWith(tile_basic, tile_grey, SB);

                    // create an exit to the right
                    Vector2 home_exit_1 = new Vector2(place_offset_x, place_offset_y / 2);// TODO Stop it with these local variables that can't be used accross case statement
                    Vector2 home_exit_2 = new Vector2(place_offset_x, (place_offset_y / 2) + 1);

                    this.PlaceTile(tile_blue, SB, home_exit_1);
                    this.PlaceTile(tile_blue, SB, home_exit_2);

                    collision_tiles.Remove(home_exit_1);
                    collision_tiles.Remove(home_exit_2);

                    transision_tiles.Add(home_exit_1);
                    transision_tiles.Add(home_exit_2);

                    break;

                case Map.level_1_1:
                    collision_tiles.Clear(); //TODO stop setting these Lists everytime you draw!!!
                    transision_tiles.Clear();


                    this.FillLvlWith(tile_basic, tile_grey, SB);

                    // create an exit to the left
                    Vector2 tt1 = new Vector2(level_offset, place_offset_y / 2);
                    Vector2 tt2 = new Vector2(level_offset, (place_offset_y / 2) + 1);

                    this.PlaceTile(tile_blue, SB, tt1);
                    this.PlaceTile(tile_blue, SB, tt2);

                    collision_tiles.Remove(tt1);
                    transision_tiles.Add(tt1);

                    collision_tiles.Remove(tt2);
                    transision_tiles.Add(tt2);

                    break;
                case Map.level_1_2:
                    break;
                case Map.level_1_3:
                    break;
            }
        }

        public void Draw(SpriteBatch SB)
        {
            switch (gs.current_state)
            {
                case StateOfGame.title:
                    break;
                case StateOfGame.play:
                    this.DrawMap(SB);
                    break;
                case StateOfGame.menu:
                    break;
                case StateOfGame.pause:
                    break;
            }

        }
    }
}