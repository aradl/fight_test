﻿using System;

namespace fight_test
{
    public enum Map
    {
        home      = 0,
        level_1_1 = 1,
        level_1_2 = 2,
        level_1_3 = 3
    }

    public enum StateOfGame
    {
        title = 0,
        play  = 1,
        menu  = 2,
        pause = 3         
    }

    class GameState
    {
        // state variables
        public StateOfGame current_state;
        public Map current_map;

        public GameState()
        {
            current_state = StateOfGame.play;
            current_map = Map.home;
        }

    }
}
