﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace fight_test
{
    class SfxController
    {
        // control
        private float volume = 0.1f;

        // SFX // TODO create a music controller
        private SoundEffect thrustIdleSFX;
        private SoundEffectInstance thrustIdleSFXI;
        private SoundEffect thrustMoveSFX;
        private SoundEffectInstance thrustMoveSFXI;

        public SfxController(float _volume)
        {
            volume = _volume;
        }

        public void LoadContent(Microsoft.Xna.Framework.Content.ContentManager CM)
        {            
            thrustIdleSFX = CM.Load<SoundEffect>("thrust_idle_sfx"); // TODO Update SFX
            thrustMoveSFX = CM.Load<SoundEffect>("thrust_move_sfx");
            thrustIdleSFXI = thrustIdleSFX.CreateInstance();
            thrustMoveSFXI = thrustMoveSFX.CreateInstance();

            thrustMoveSFXI.IsLooped = true;
            thrustIdleSFXI.Volume = volume;
            thrustIdleSFXI.IsLooped = true;            
        }

        public void playIdle()
        {
            thrustIdleSFXI.Play();
            thrustMoveSFXI.Stop();

        }

        public void playMove()
        {
            thrustIdleSFXI.Stop();
            thrustMoveSFXI.Play();

        }

    }
}
